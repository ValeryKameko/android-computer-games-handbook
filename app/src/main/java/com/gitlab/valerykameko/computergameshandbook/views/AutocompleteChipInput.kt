package com.gitlab.valerykameko.computergameshandbook.views

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.view.children
import com.gitlab.valerykameko.computergameshandbook.R
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.chip.Chip


abstract class AutocompleteChipInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
): LinearLayout(context, attrs, defStyle) {
    fun interface ValueChangedListener {
        fun onValueChange(view: AutocompleteChipInput, value: List<String>?)
    }
    fun interface ChipFactory {
        fun createChip(context: Context, value: String): Chip
    }

    protected abstract val chipFactory: ChipFactory
    private var listener: ValueChangedListener? = null
    private val values = mutableSetOf<String>()
    private val autocompleteStrings = mutableListOf<String>()
    private val autocompleteTagsAdapter = ArrayAdapter(context, R.layout.item_text_view, autocompleteStrings)

    private val valuesInputAutoCompleteTextView: AutoCompleteTextView by lazy { findViewById(R.id.tagsInputAutoCompleteTextView) }
    private val valuesInputLayout: FlexboxLayout by lazy { findViewById(R.id.tagsInputLayout) }

    init {
        inflate(context, R.layout.view_autocomplete_chips_input, this)
        val a = context.obtainStyledAttributes(attrs, R.styleable.AutocompleteChipInput)
        valuesInputAutoCompleteTextView.hint = a.getString(R.styleable.AutocompleteChipInput_hint)
        valuesInputAutoCompleteTextView.setOnEditorActionListener(this::handleTagInput)
        valuesInputAutoCompleteTextView.setAdapter(autocompleteTagsAdapter)
        a.recycle()
    }

    fun setValues(values: List<String>?) {
        val newValues = values ?: emptyList()
        val addedValues = newValues.subtract(this.values)
        val deletedValues = this.values.subtract(newValues)
        addedValues.forEach(this::addValue)
        deletedValues.forEach(this::removeTag)
    }

    fun getValues(): List<String> =
        this.values.toList()

    fun setListener(listener: ValueChangedListener?) {
        this.listener = listener
    }

    fun setAutocompleteTags(autocompleteValues: List<String>?) {
        autocompleteStrings.clear()
        autocompleteTagsAdapter.clear()
        autocompleteValues?.let(autocompleteStrings::addAll)
        autocompleteTagsAdapter.addAll(autocompleteStrings)
        autocompleteTagsAdapter.notifyDataSetChanged();
    }

    fun addTextChangedListener(listener: (String) -> Unit) {
        valuesInputAutoCompleteTextView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

            override fun afterTextChanged(s: Editable?) = listener(s?.toString() ?: "")
        })
    }

    private fun addValue(value: String) {
        if (values.add(value)) {
            val chip = chipFactory.createChip(context, value).apply {
                isCloseIconVisible = true
                isCheckable = false
                isClickable = true
                setOnCloseIconClickListener { removeTag(value) }
            }
            valuesInputLayout.addView(chip, childCount - 1)
            listener?.onValueChange(this, values.toList())
        }
    }

    private fun removeTag(value: String) {
        if (values.remove(value)) {
            val chip = valuesInputLayout.children.asSequence()
                .filterIsInstance<Chip>()
                .find { tagChip -> tagChip.text == value }
            valuesInputLayout.removeView(chip)
            listener?.onValueChange(this, values.toList())
        }
    }

    private fun handleTagInput(v: TextView, actionId: Int, event: KeyEvent? = null): Boolean {
        return when(actionId) {
            EditorInfo.IME_ACTION_DONE -> {
                if (!v.text.isNullOrEmpty()) {
                    addValue(v.text.toString())
                    valuesInputAutoCompleteTextView.setText("")
                } else {
                    hideKeyboard()
                }
                true
            }
            else -> false
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
    }
}