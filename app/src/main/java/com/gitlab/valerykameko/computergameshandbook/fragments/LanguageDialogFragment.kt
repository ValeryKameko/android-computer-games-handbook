package com.gitlab.valerykameko.computergameshandbook.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ListView
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.adapters.FontEntriesArrayAdapter
import com.gitlab.valerykameko.computergameshandbook.data.model.Language
import com.gitlab.valerykameko.computergameshandbook.databinding.DialogFontSpinnerBinding
import com.gitlab.valerykameko.computergameshandbook.databinding.DialogLanguageSpinnerBinding
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel

class LanguageDialogFragment(private val languages: List<Language>): DialogFragment() {
    fun interface LanguageChoiceListener {
        fun onLanguageChoose(language: Language)
    }

    private var _listener: LanguageChoiceListener? = null

    fun setListener(listener: LanguageChoiceListener) {
        _listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = DataBindingUtil.inflate<DialogLanguageSpinnerBinding>(
            LayoutInflater.from(requireContext()), R.layout.dialog_language_spinner, null, false
        ).apply {
            lifecycleOwner = requireActivity()
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())

            val adapter = ArrayAdapter<Language>(requireContext(), R.layout.item_text_view, languages)
            languagesListView.adapter = adapter

            languagesListView.setOnItemClickListener { _, _, position, _ ->
                _listener?.onLanguageChoose(adapter.getItem(position)!!)
                dialog?.dismiss()
            }
        }

        val builder = AlertDialog.Builder(activity)
            .setTitle(R.string.dialog_font_spinner_select_font)
            .setView(binding.root)
            .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
        return builder.create()
    }
}