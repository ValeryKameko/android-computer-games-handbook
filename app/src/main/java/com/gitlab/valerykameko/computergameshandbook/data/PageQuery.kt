package com.gitlab.valerykameko.computergameshandbook.data

data class PageQuery(
    val page: Int,
    val perPage: Int,
)
