package com.gitlab.valerykameko.computergameshandbook.services

import android.net.Uri
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

internal class UriTypeAdapter : TypeAdapter<Uri>() {
    override fun write(out: JsonWriter, value: Uri?) {
        when (value) {
            null -> out.nullValue()
            else -> out.value(value.toString())
        }
    }

    override fun read(`in`: JsonReader): Uri? {
        return when (`in`.peek()) {
            JsonToken.NULL -> `in`.nextNull().let { null }
            else -> Uri.parse(`in`.nextString())
        }
    }
}