package com.gitlab.valerykameko.computergameshandbook.views

import android.content.Context
import android.util.AttributeSet
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.google.android.material.chip.Chip

class PlatformChip(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = R.attr.chipStyle,
) : Chip(context, attrs, defStyle) {
    constructor(context: Context, platform: Platform) : this(context) {
        inflate(context, R.layout.item_platform_chip, null)
        text = platform.name
    }
}