package com.gitlab.valerykameko.computergameshandbook.services.local

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.core.net.toFile
import androidx.core.net.toUri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.security.MessageDigest

class MediaLocalService internal constructor(
    directory: File,
) {
    companion object {
        private const val IMAGES_DIRECTORY = "image"
        private const val CLIPS_DIRECTORY = "clip"
        private const val MIN_HASH_LENGTH = 20
        private const val DEFAULT_HASH_ALGORITHM = "SHA-1"
        private val FILENAME_PREFIX_UNFOLD_RANGE = 0..2
        private val HASH_REGEX = Regex("([a-f0-9]{${MIN_HASH_LENGTH},})")
    }

    private val imageDirectory = File(directory, IMAGES_DIRECTORY)
    private val imageUris: MutableSet<Uri> by lazy {
        loadImages(imageDirectory.toUri())
    }

    private val clipDirectory = File(directory, CLIPS_DIRECTORY)
    private val clipUris: MutableSet<Uri> by lazy {
        loadClips(clipDirectory.toUri()).also {
            Timber.d("Loaded clips $it")
        }
    }

    suspend fun getImageUri(uri: Uri): Uri? = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        if (imageUris.contains(imageUri)) {
            imageUri
        } else {
            null
        }
    }

    suspend fun getImageData(uri: Uri): ByteArray? = withContext(Dispatchers.IO) {
        Timber.d("Load image $uri")
        val imageUri = encodeImageUri(uri)
        if (!imageUris.contains(imageUri)) {
            null
        } else {
            try {
                imageUri.toFile().readBytes()
            } catch (ex: NoSuchFileException) {
                Timber.e("Image load error $ex")
                null
            }
        }
    }

    suspend fun addImage(uri: Uri, data: ByteArray) = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        imageUris.add(imageUri)

        imageUri.toFile().apply {
            parentFile?.mkdirs()
            createNewFile()
            writeBytes(data)
            Timber.i("Created local image ${path}")
        }
    }

    suspend fun removeImage(uri: Uri) = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        if (imageUris.remove(imageUri)) {
            imageUri.toFile().delete()
        }
    }

    suspend fun getClipUri(uri: Uri): Uri? = withContext(Dispatchers.IO) {
        val clipUri = encodeClipUri(uri)
        if (clipUris.contains(clipUri)) {
            clipUri
        } else {
            null
        }
    }

    suspend fun getClipData(uri: Uri): ByteArray? = withContext(Dispatchers.IO) {
        Timber.d("Load clip $uri")
        val clipUri = encodeClipUri(uri)
        if (!clipUris.contains(clipUri)) {
            null
        } else {
            try {
                clipUri.toFile().readBytes()
            } catch (ex: NoSuchFileException) {
                Timber.e("Clip load error $ex")
                null
            }
        }
    }

    suspend fun addClip(uri: Uri, data: ByteArray) = withContext(Dispatchers.IO) {
        val clipUri = encodeClipUri(uri)
        clipUris.add(clipUri)

        clipUri.toFile().apply {
            parentFile?.mkdirs()
            createNewFile()
            writeBytes(data)
            Timber.i("Created local clip ${path}")
        }
    }

    suspend fun removeClip(uri: Uri) = withContext(Dispatchers.IO) {
        val clipUri = encodeClipUri(uri)
        if (clipUris.remove(clipUri)) {
            clipUri.toFile().delete()
        }
    }

    private fun encodeImageUri(uri: Uri): Uri {
        val relativeUri = encodeRelativeUri(uri)
        val imageDirectoryUri = imageDirectory.toUri()
        return imageDirectoryUri.buildUpon()
            .encodedPath(imageDirectoryUri.path + relativeUri.path)
            .build()
    }

    private fun encodeClipUri(uri: Uri): Uri {
        val relativeUri = encodeRelativeUri(uri)
        val clipDirectoryUri = clipDirectory.toUri()
        return clipDirectoryUri.buildUpon()
            .encodedPath(clipDirectoryUri.path + relativeUri.path)
            .build()
    }

    private fun encodeRelativeUri(uri: Uri): Uri {
        val hash = extractHash(uri) ?: {
            val uriByteArray = uri.toString().toByteArray()
            val hashByteArray =
                MessageDigest.getInstance(DEFAULT_HASH_ALGORITHM).digest(uriByteArray)
            hashByteArray.joinToString(separator = "") { byte -> String.format("%02x", byte) }
        }()
        return Uri.Builder()
            .appendPath(hash.substring(FILENAME_PREFIX_UNFOLD_RANGE))
            .appendPath("$hash.${extractExtension(uri)}")
            .build()
    }

    private fun extractHash(uri: Uri): String? {
        val hashes = HASH_REGEX.findAll(uri.path ?: "")
        val hash = hashes.firstOrNull()
        return hash?.value
    }

    private fun extractExtension(uri: Uri): String? =
        uri.lastPathSegment?.substringAfterLast('.', "")

    private fun loadImages(path: Uri): MutableSet<Uri> {
        val imageUris = mutableSetOf<Uri>()
        val file: File = path.toFile()
        file.walk().forEach { childFile ->
            if (childFile.isFile) {
                imageUris.add(childFile.toUri())
            }
        }
        return imageUris
    }

    private fun loadClips(path: Uri): MutableSet<Uri> {
        val clipUris = mutableSetOf<Uri>()
        val file: File = path.toFile()
        file.walk().forEach { childFile ->
            if (childFile.isFile) {
                clipUris.add(childFile.toUri())
            }
        }
        return clipUris
    }
}

