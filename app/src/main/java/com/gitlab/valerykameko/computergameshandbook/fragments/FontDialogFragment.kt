package com.gitlab.valerykameko.computergameshandbook.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ListView
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.adapters.FontEntriesArrayAdapter
import com.gitlab.valerykameko.computergameshandbook.databinding.DialogFontSpinnerBinding
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel

class FontDialogFragment(private val fontEntries: List<FontEntry>): DialogFragment() {
    fun interface FontChoiceListener {
        fun onFontChoose(fontEntry: FontEntry)
    }

    private var _listener: FontChoiceListener? = null

    fun setListener(listener: FontChoiceListener) {
        _listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = DataBindingUtil.inflate<DialogFontSpinnerBinding>(
            LayoutInflater.from(requireContext()), R.layout.dialog_font_spinner, null, false
        ).apply {
            lifecycleOwner = requireActivity()
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())

            val adapter = FontEntriesArrayAdapter(requireContext(), R.layout.item_text_view, fontEntries)
            fontsListView.adapter = adapter

            fontValueSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    adapter.filter.filter(query)
                    adapter.notifyDataSetChanged()
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    adapter.filter.filter(newText)
                    adapter.notifyDataSetChanged()
                    return false
                }
            })

            fontsListView.setOnItemClickListener { _, _, position, _ ->
                _listener?.onFontChoose(adapter.getItem(position))
                dialog?.dismiss()
            }
        }

        val builder = AlertDialog.Builder(activity)
            .setTitle(R.string.dialog_font_spinner_select_font)
            .setView(binding.root)
            .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
        return builder.create()
    }
}