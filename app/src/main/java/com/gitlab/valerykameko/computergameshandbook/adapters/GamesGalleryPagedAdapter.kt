package com.gitlab.valerykameko.computergameshandbook.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.MediaStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.gitlab.valerykameko.computergameshandbook.databinding.ItemGalleryGameBinding
import com.gitlab.valerykameko.computergameshandbook.fragments.GamesGalleryFragmentDirections
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GalleryGameViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel

class GamesGalleryPagedAdapter(private val mediaStorage: MediaStorage) :
    PagingDataAdapter<Game, GamesGalleryPagedAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemGalleryGameBinding>(
            LayoutInflater.from(parent.context), R.layout.item_gallery_game, parent, false
        )
        return ViewHolder(binding, mediaStorage).also { holder ->
            binding.lifecycleOwner = holder.itemView.context as LifecycleOwner
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.apply(holder::bind)
    }

    class ViewHolder(
        private val binding: ItemGalleryGameBinding,
        private val mediaStorage: MediaStorage,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(game: Game) {
            with(binding) {
                gameViewModel = GalleryGameViewModel(game, mediaStorage)
                globalSettingsViewModel = GlobalSettingsViewModel.getInstance(itemView.context)

                itemView.setOnClickListener { view ->
                    gameViewModel?.let { gameViewModel ->
                        navigateToGame(gameViewModel.id, view)
                    }
                }
            }
        }

        private fun navigateToGame(id: String, view: View) {
            val direction =
                GamesGalleryFragmentDirections.actionGamesGalleryFragmentToGameDetailsFragment(id)
            view.findNavController().navigate(direction)
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Game>() {
            override fun areItemsTheSame(oldItem: Game, newItem: Game) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Game, newItem: Game) =
                oldItem == newItem
        }
    }
}
