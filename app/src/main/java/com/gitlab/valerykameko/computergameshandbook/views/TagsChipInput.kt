package com.gitlab.valerykameko.computergameshandbook.views

import android.content.Context
import android.util.AttributeSet
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.gitlab.valerykameko.computergameshandbook.views.AutocompleteChipInput.ChipFactory

class TagsChipInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
): AutocompleteChipInput(context, attrs, defStyle) {
    override val chipFactory = ChipFactory { context, value -> TagChip(context, Tag(value)) }
}

