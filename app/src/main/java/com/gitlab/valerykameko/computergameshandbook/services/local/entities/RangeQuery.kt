package com.gitlab.valerykameko.computergameshandbook.services.local.entities

data class RangeQuery(
    val offset: Int? = null,
    val count: Int? = null,
)