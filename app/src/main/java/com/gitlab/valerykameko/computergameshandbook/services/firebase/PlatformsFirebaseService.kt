package com.gitlab.valerykameko.computergameshandbook.services.firebase

import androidx.collection.LruCache
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.PlatformView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.PlatformsQueryView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.TagView
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class PlatformsFirebaseService(
    firebaseFirestore: FirebaseFirestore
) {
    private val platformsCollection = firebaseFirestore.collection("platforms")
    private val cache: LruCache<String, PlatformView> = LruCache(10)

    fun getPlatforms(query: PlatformsQueryView?): Flow<List<PlatformView>> = flow {
        var firebaseQuery: Query = platformsCollection

        query?.let {
            firebaseQuery = query.applyQuery(firebaseQuery)
        }

        val platformsSnapshot = firebaseQuery.get().await()
        val platformViews = platformsSnapshot.mapNotNull { deserializePlatform(it) }

        emit(platformViews)
    }

    fun getPlatform(id: String): Flow<PlatformView?> = flow {
        val platformView = run {
            val kek = cache.get(id)
            kek?.also {
                Timber.i("Loaded platform from cache $kek")
            }
        } ?: run {
            val platformDocument = platformsCollection.document(id)
            val platformSnapshot = platformDocument.get().await()
            val platformView = deserializePlatform(platformSnapshot)

            Timber.i("Loaded platform $platformView")
            platformView?.also {
                Timber.i("Cache platform $id -> $platformView")
                cache.put(id, platformView)
            }
        }

        emit(platformView)
    }

    suspend fun addPlatform(platform: PlatformView): DocumentReference {
        val samePlatformsQuery = platformsCollection.whereEqualTo("name", platform.name)
        val samePlatformsSnapshot = samePlatformsQuery.get().await()
        samePlatformsSnapshot.forEach { samePlatformSnapshot ->
            if (samePlatformSnapshot.exists())
                return samePlatformSnapshot.reference
        }

        val platformSnapshot = serializePlatform(platform)
        return platformsCollection.add(platformSnapshot).await()
    }

    private fun deserializePlatform(document: DocumentSnapshot): PlatformView? {
        if (!document.exists())
            return null
        return PlatformView(
            name = document.getString("name")!!
        )
    }

    private fun serializePlatform(platform: PlatformView): Map<String, Any> {
        return mapOf(
            "name" to platform.name
        )
    }
}
