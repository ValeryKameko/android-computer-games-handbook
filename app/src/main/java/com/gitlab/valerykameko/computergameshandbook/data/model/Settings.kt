package com.gitlab.valerykameko.computergameshandbook.data.model

import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry

data class Settings(
    val fontEntry: FontEntry,
    val language: Language,
)