package com.gitlab.valerykameko.computergameshandbook.services

import android.content.Context
import com.gitlab.valerykameko.computergameshandbook.services.local.LocalService

object LocalServiceFactory {
    @Volatile
    private var localService: LocalService? = null

    fun getLocalService(context: Context) =
        localService ?: synchronized(this) {
            localService ?: LocalService(context).also { localService = it }
        }

    fun getGamesLocalService(context: Context) = getLocalService(context).gamesService
    fun getMediaLocalService(context: Context) = getLocalService(context).mediaService
    fun getTagsLocalService(context: Context) = getLocalService(context).tagsService
    fun getGenresLocalService(context: Context) = getLocalService(context).genresService
    fun getPlatformsLocalService(context: Context) = getLocalService(context).platformsService
}