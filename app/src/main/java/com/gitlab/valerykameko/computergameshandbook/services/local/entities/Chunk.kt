package com.gitlab.valerykameko.computergameshandbook.services.local.entities

data class Chunk<T>(
    val count: Int,
    val items: List<T>,
)