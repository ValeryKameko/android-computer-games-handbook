package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.google.gson.TypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

@JsonAdapter(TagView.GenreJsonAdapter::class)
data class TagView(
    val value: String,
) {
    class GenreJsonAdapter : TypeAdapter<TagView>() {
        override fun write(out: JsonWriter, value: TagView) {
            out.value(value.value)
        }

        override fun read(`in`: JsonReader): TagView {
            val name = `in`.nextString()
            return TagView(name)
        }
    }

    companion object {
        fun fromView(view: TagView) = Tag(view.value)
        fun toView(tag: Tag) = TagView(tag.value)
    }
}

