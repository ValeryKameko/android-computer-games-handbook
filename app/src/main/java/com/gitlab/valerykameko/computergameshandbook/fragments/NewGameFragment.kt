package com.gitlab.valerykameko.computergameshandbook.fragments

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.databinding.FragmentNewGameBinding
import com.gitlab.valerykameko.computergameshandbook.util.tryOptional
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.NewGameViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

@ExperimentalCoroutinesApi
class NewGameFragment : Fragment() {
    companion object {
        private const val PICK_IMAGE_CODE = 0
    }
    private lateinit var binding: FragmentNewGameBinding

    private var loadTagsJob: Job? = null
    private var loadGenresJob: Job? = null
    private var loadPlatformsJob: Job? = null

    private val viewModel: NewGameViewModel by viewModels {
        NewGameViewModel.Factory(
            StorageFactory.getGamesStorage(requireContext()),
            StorageFactory.getMediaStorage(requireContext()),
            StorageFactory.getTagsStorage(requireContext()),
            StorageFactory.getGenresStorage(requireContext()),
            StorageFactory.getPlatformsStorage(requireContext()),
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        DataBindingUtil.inflate<FragmentNewGameBinding>(
            inflater, R.layout.fragment_new_game, container, false
        ).apply {
            binding = this
            newGameViewModel = viewModel
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())
            lifecycleOwner = viewLifecycleOwner

            toolbar.setNavigationOnClickListener { view -> view.findNavController().navigateUp() }
            releasedValueEditText.setOnClickListener {
                val calendar = Calendar.getInstance()
                DatePickerDialog(requireContext(), { view, year, month, dayOfMonth ->
                    viewModel.releasedAt.value = calendar.apply { set(year, month, dayOfMonth) }.time
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                    .show()
            }
            imageView.setOnClickListener {
                val getIntent = Intent(Intent.ACTION_GET_CONTENT)
                    .setType("image/*")
                val pickIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    .setType("image/*")
                val chooserIntent = Intent.createChooser(getIntent, "Select image")
                    .apply { putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent)) }
                startActivityForResult(chooserIntent, PICK_IMAGE_CODE)
            }
            createButton.setOnClickListener { view ->
                createGame(view)
            }
            tagsInput.addTextChangedListener(this@NewGameFragment::loadTags)
            genresInput.addTextChangedListener(this@NewGameFragment::loadGenres)
            platformsInput.addTextChangedListener(this@NewGameFragment::loadPlatforms)
        }
        return binding.root
    }

    private fun createGame(view: View) {
        val title = viewModel.title.value
        if (title.isNullOrEmpty()) return showError("Title is empty")

        viewModel.releasedAt.value
            ?: return showError("Released at is empty")

        viewModel.addedCount.value
            ?: return showError("Added By is empty")

        val websiteUriString = viewModel.websiteUriString.value
        if (!websiteUriString.isNullOrBlank()) {
            val websiteUri = tryOptional { Uri.parse(websiteUriString) }
                ?: return showError("Website URI format error")
            viewModel.websiteUri.value = websiteUri
        }

        binding.createButton.isEnabled = false
        binding.progressBar.visibility = View.VISIBLE
        lifecycleScope.launch {
            val gameId = viewModel.createGame()
            val direction = NewGameFragmentDirections.actionNewGameFragmentToGameDetailsFragment(gameId)
            view.findNavController().navigate(direction)
        }
    }

    private fun loadTags(prefix: String) {
        loadTagsJob?.cancel()
        loadTagsJob = lifecycleScope.launch {
            viewModel.loadAutocompleteTags(prefix)
        }
    }

    private fun loadGenres(prefix: String) {
        loadGenresJob?.cancel()
        loadGenresJob = lifecycleScope.launch {
            viewModel.loadAutocompleteGenres(prefix)
        }
    }

    private fun loadPlatforms(prefix: String) {
        loadPlatformsJob?.cancel()
        loadPlatformsJob = lifecycleScope.launch {
            viewModel.loadAutocompletePlatforms(prefix)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == PICK_IMAGE_CODE && resultCode == Activity.RESULT_OK -> {
                val uri = data?.data
                if (data == null || uri == null) return showError(getString(R.string.generic_error))

                val stream = requireContext().contentResolver.openInputStream(uri)
                    ?: return showError(getString(R.string.generic_error))

                viewModel.bitmap.value = BitmapFactory.decodeStream(stream)
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun showError(error: String) {
        val snackbar = Snackbar.make(
            requireView().rootView, error, Snackbar.LENGTH_SHORT
        )
        snackbar.show()
    }
}
