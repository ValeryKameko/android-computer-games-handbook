package com.gitlab.valerykameko.computergameshandbook.adapters

import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.gitlab.valerykameko.computergameshandbook.util.tryOptional
import com.gitlab.valerykameko.computergameshandbook.views.GenreChip
import com.google.android.material.chip.ChipGroup

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("isGone")
    fun View.bindIsGone(isGone: Boolean) {
        visibility = if (isGone) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    @JvmStatic
    @BindingAdapter("htmlText")
    fun TextView.bindHtmlText(text: String?) {
        text?.let {
            this.text = HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    @JvmStatic
    @BindingAdapter("bitmap")
    fun ImageView.bindImageFromUri(bitmap: Bitmap?) {
        if (bitmap == null) {
            setImageResource(R.drawable.image_not_found)
        } else {
            setImageBitmap(bitmap)
        }
    }

    @JvmStatic
    @BindingAdapter("genres")
    fun ChipGroup.bindGenres(genres: List<Genre>?) {
        genres?.onEach { genre ->
            val chip = GenreChip(context, genre)
            addView(chip)
        }
    }

    @JvmStatic
    @BindingAdapter("fontFamily")
    fun View.bindFontFamily(typeface: Typeface?) {
        if (typeface != null) {
            setCustomTypeface(typeface)
        }
    }
}
