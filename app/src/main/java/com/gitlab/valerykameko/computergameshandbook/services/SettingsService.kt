package com.gitlab.valerykameko.computergameshandbook.services

import android.content.Context
import android.net.Uri
import com.gitlab.valerykameko.computergameshandbook.services.entities.SettingsView
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

class SettingsService(context: Context) {
    companion object {
        private const val SETTINGS_FILE = "settings.json"
    }
    private val settingsViewType = object : TypeToken<SettingsView>() {}.type
    private val settingsViewGson = GsonBuilder()
            .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
            .create()

    private val file = File(context.filesDir, SETTINGS_FILE)

    fun saveSettings(settingsView: SettingsView) {
        file.outputStream().let { stream ->
            JsonWriter(stream.writer()).use { writer ->
                settingsViewGson.toJson(settingsView, settingsViewType, writer)
            }
        }
    }

    fun loadSettings(): SettingsView? {
        try {
            file.inputStream().let { stream ->
                JsonReader(stream.reader()).use { reader ->
                    return settingsViewGson.fromJson(reader, settingsViewType)
                }
            }
        } catch (ex: Exception) {
            Timber.e("Settings loading error $ex")
            return null
        }
    }
}
