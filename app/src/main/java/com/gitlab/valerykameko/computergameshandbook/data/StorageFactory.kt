package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context

object StorageFactory {
   @Volatile
   private var storage: Storage? = null

   private fun getStorage(context: Context): Storage =
      storage ?: synchronized(this) {
         storage ?: Storage(context).also { storage = it }
      }

   fun getQueryStorage(context: Context) = getStorage(context).queryStorage
   fun getSettingsStorage(context: Context) = getStorage(context).settingsStorage
   fun getPlatformsStorage(context: Context) = getStorage(context).platformsStorage
   fun getGenresStorage(context: Context) = getStorage(context).genresStorage
   fun getGamesStorage(context: Context) = getStorage(context).gamesStorage
   fun getMediaStorage(context: Context) = getStorage(context).mediaStorage
   fun getTagsStorage(context: Context) = getStorage(context).tagsStorage

}