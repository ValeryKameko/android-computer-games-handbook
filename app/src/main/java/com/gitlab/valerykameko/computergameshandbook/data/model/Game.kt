package com.gitlab.valerykameko.computergameshandbook.data.model

import android.net.Uri
import java.util.*

data class Game(
    val id: String = "",
    val name: String,
    val rating: Double,
    val releasedDate: Date,
    val addedCount: Long,
    val platforms: List<Platform>,
    val tags: List<Tag>,
    val genres: List<Genre>,
    val description: String,
    val websiteUri: Uri?,
    val imageUri: Uri?,
    val clipUri: Uri?,
) {
    companion object {
        val TEST_INSTANCE = Game(
            "id",
            "Dota 3",
            4.4,
            GregorianCalendar(2017, GregorianCalendar.APRIL, 2).time,
            1324123,
            listOf(Platform("Windows"), Platform("Linux"), Platform("Android"), Platform("Kek")),
            listOf(Tag("Cool"), Tag("WOW!!")),
            listOf(Genre("RPG"), Genre("Action")),
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae congue quam, ut aliquet sapien. Sed quis lorem sed sem gravida rhoncus nec sit amet diam. Donec dolor est, scelerisque vel tincidunt eget, placerat eu leo. Quisque sit amet urna varius, interdum lorem ac, egestas metus. Praesent in nibh in purus finibus vulputate. Aliquam diam nulla, elementum sit amet metus eget, pellentesque pellentesque arcu. Vestibulum molestie consectetur nisi, ut convallis nunc dictum eget. Etiam lobortis, mi volutpat euismod consequat, sapien nunc rutrum lorem, et feugiat tellus diam non nulla.\n" +
                    "\n" +
                    "Nunc suscipit sit amet massa non pharetra. Vestibulum dignissim sodales tellus, et pharetra lacus feugiat eu. Donec at libero nunc. Morbi vitae nisl nulla. Phasellus commodo non diam eget malesuada. Nulla justo velit, interdum vitae neque quis, maximus ornare nisi. Duis a dictum nisl. Pellentesque in lorem vel ex malesuada interdum nec vitae justo. Sed lacus tellus, fringilla laoreet erat eget, ultrices lobortis velit. Aliquam quis ipsum sapien. Nunc commodo molestie lobortis. Suspendisse lorem mi, sodales vel neque dictum, tempor suscipit eros. Pellentesque et justo venenatis, hendrerit elit nec, sodales orci. Integer sed ipsum est. Sed tempor tristique pretium. In euismod tempus arcu, ac egestas augue.",
            Uri.parse("https://www.google.com"),
            null,
            null
        )
    }
}
