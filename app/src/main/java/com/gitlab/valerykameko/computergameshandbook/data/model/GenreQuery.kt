package com.gitlab.valerykameko.computergameshandbook.data.model

data class GenreQuery(
    val prefix: String? = null,
    val count: Int,
)