package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Genre

data class GenreView(
    val name: String,
) {
    companion object {
        fun fromView(view: GenreView) = Genre(view.name)
        fun toView(genre: Genre) = GenreView(genre.name)
    }
}

