package com.gitlab.valerykameko.computergameshandbook.services

import android.content.Context
import com.gitlab.valerykameko.computergameshandbook.services.firebase.FirebaseService

object FirebaseServiceFactory {
    @Volatile
    private var firebaseService: FirebaseService? = null

    fun getFirebaseService(context: Context) =
        firebaseService ?: synchronized(this) {
            firebaseService ?: FirebaseService(context).also { firebaseService = it }
        }

    fun getGamesFirebaseService(context: Context) = getFirebaseService(context).gamesService
    fun getMediaFirebaseService(context: Context) = getFirebaseService(context).mediaService
    fun getTagsFirebaseService(context: Context) = getFirebaseService(context).tagsService
    fun getGenresFirebaseService(context: Context) = getFirebaseService(context).genresService
    fun getPlatformsFirebaseService(context: Context) = getFirebaseService(context).platformsService
}