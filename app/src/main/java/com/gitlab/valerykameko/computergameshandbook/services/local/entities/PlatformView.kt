package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.google.gson.TypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

@JsonAdapter(PlatformView.GenreJsonAdapter::class)
data class PlatformView(
    val name: String,
) {
    class GenreJsonAdapter : TypeAdapter<PlatformView>() {
        override fun write(out: JsonWriter, value: PlatformView) {
            value.apply {
                out.value(name)
            }
        }

        override fun read(`in`: JsonReader): PlatformView {
            val name = `in`.nextString()
            return PlatformView(name)
        }
    }

    companion object {
        fun fromView(view: PlatformView) = Platform(view.name)
        fun toView(platform: Platform) = PlatformView(platform.name)
    }
}

