package com.gitlab.valerykameko.computergameshandbook.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.gitlab.valerykameko.computergameshandbook.data.model.GameField
import com.gitlab.valerykameko.computergameshandbook.util.Translatable

class TranslatableArrayAdapter(
    context: Context,
    textViewResourceId: Int,
    private val values: List<Translatable>,
) : ArrayAdapter<Translatable>(context, textViewResourceId, values) {

    override fun getCount() = values.size
    override fun getItem(position: Int) = values[position]
    fun getIndex(value: Translatable?): Int? = values.indexOf(value).let { index ->
        when (index) {
            -1 -> null
            else -> index
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        label.text = context.resources.getString(values[position].translation)
        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getDropDownView(position, convertView, parent) as TextView
        label.text = context.resources.getString(values[position].translation)
        return label
    }
}
