package com.gitlab.valerykameko.computergameshandbook.services

import android.content.Context
import android.graphics.Typeface
import android.net.Uri
import androidx.core.net.toUri
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry
import timber.log.Timber
import java.io.File

class FontsService(context: Context) {
    companion object {
        private val FONTS_DIRECTORIES: List<String> = listOf(
            "/system/fonts", "/system/font", "/data/fonts", "//android_asset/fonts",
        )
    }

    private val fonts: List<FontEntry> by lazy(this::loadFonts)

    fun getAvailableFonts(): List<FontEntry> {
        Timber.v("Available fonts $fonts")
        return fonts
    }

    fun getFont(font: Uri): FontEntry? =
        fonts.find { fontEntry -> fontEntry.uri == font }

    private fun loadFonts(): List<FontEntry> = FONTS_DIRECTORIES.flatMap { directory ->
        loadFonts(File(directory))
    }

    private fun loadFonts(file: File): List<FontEntry> {
        return if (!file.exists())
            emptyList()
        else if (file.isFile) {
            try {
                listOf(FontEntry(
                    name = file.nameWithoutExtension,
                    typeface = Typeface.createFromFile(file),
                    uri = file.toUri(),
                ))
            } catch (ex: Exception) {
                emptyList()
            }
        } else {
            file.listFiles()?.flatMap { nestedFile ->
                loadFonts(nestedFile)
            } ?: emptyList()
        }
    }
}