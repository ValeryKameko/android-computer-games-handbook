package com.gitlab.valerykameko.computergameshandbook

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.gitlab.valerykameko.computergameshandbook.data.SettingsStorage
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.util.LanguageContextWrapper
import timber.log.Timber

class ComputersHandbookActivity : AppCompatActivity() {
    private val settingsStorage: SettingsStorage by lazy {
        StorageFactory.getSettingsStorage(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.decorView.apply {
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        setContentView(R.layout.activity_computers_handbook)
        settingsStorage.setListener {
            recreate()
        }

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            window.decorView.apply {
                systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            }
        }
    }

    override fun attachBaseContext(newContext: Context) {
        val settingsStorage: SettingsStorage = StorageFactory.getSettingsStorage(newContext)
        val settings = settingsStorage.getSettings().value!!
        Timber.d("Change to $settings")

        super.attachBaseContext(LanguageContextWrapper.wrap(newContext, settings.language))
    }
}