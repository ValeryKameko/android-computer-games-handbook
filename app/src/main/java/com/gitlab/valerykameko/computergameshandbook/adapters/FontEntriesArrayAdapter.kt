package com.gitlab.valerykameko.computergameshandbook.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry
import kotlin.collections.ArrayList

class FontEntriesArrayAdapter(
    context: Context,
    textViewResourceId: Int,
    private val values: List<FontEntry>,
) : ArrayAdapter<FontEntry?>(context, textViewResourceId, values) {
    private val originalValues = values.toList()
    private var filteredValues = values
    private val _filter by lazy(this::FontsFilter)

    override fun getFilter(): Filter = _filter

    init {
        setDropDownViewResource(textViewResourceId)
    }

    override fun getCount() = values.size
    override fun getItem(position: Int) = values[position]
    fun getIndex(value: FontEntry?): Int? = values.indexOf(value).let { index ->
        when (index) {
            -1 -> null
            else -> index
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        label.text = values[position].name
        label.typeface = values[position].typeface
        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getDropDownView(position, convertView, parent) as TextView
        label.text = values[position].name
        label.typeface = values[position].typeface
        return label
    }

    private inner class FontsFilter : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val results = FilterResults()
            val locale = context.resources.configuration.locales[0]
            val prefix = constraint.toString().toLowerCase(locale)

            val filteredValues = originalValues.filter { fontEntry ->
                fontEntry.name.toLowerCase(locale).startsWith(prefix)
            }

            results.values = filteredValues
            results.count = filteredValues.size
            return results
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            notifyDataSetChanged()
            clear()
            filteredValues = results.values as List<FontEntry>
            filteredValues.forEach { fontEntry ->
                add(fontEntry)
                notifyDataSetInvalidated()
            }
        }
    }
}
