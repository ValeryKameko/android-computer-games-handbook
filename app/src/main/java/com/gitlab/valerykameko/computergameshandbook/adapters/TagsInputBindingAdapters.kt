package com.gitlab.valerykameko.computergameshandbook.adapters

import androidx.databinding.*
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.gitlab.valerykameko.computergameshandbook.views.AutocompleteChipInput
import com.gitlab.valerykameko.computergameshandbook.views.TagsChipInput

@InverseBindingMethods(
    InverseBindingMethod(type = TagsChipInput::class, attribute = "tags"),
)
object TagsInputBindingAdapters {
    @JvmStatic
    @BindingAdapter("tags")
    fun AutocompleteChipInput.bindTags(tags: List<Tag>?) =
        setValues(tags?.map(Tag::value))

    @JvmStatic
    @InverseBindingAdapter(attribute = "tags")
    fun AutocompleteChipInput.inverseBindTags(): List<Tag> =
        getValues().map(::Tag)

    @JvmStatic
    @BindingAdapter("tagsAttrChanged")
    fun AutocompleteChipInput.bindTagsListener(inverseBindingListener: InverseBindingListener?) =
        setListener { _, _ -> inverseBindingListener?.onChange() }

    @JvmStatic
    @BindingAdapter("autocompleteTags")
    fun AutocompleteChipInput.bindAutocompleteTags(autocompleteTags: List<Tag>?) =
        setAutocompleteTags(autocompleteTags?.map(Tag::value))
}