package com.gitlab.valerykameko.computergameshandbook.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.databinding.FragmentGamesFilterBinding
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GamesFilterViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel
import com.gitlab.valerykameko.computergameshandbook.views.GenresChipInput
import com.gitlab.valerykameko.computergameshandbook.views.PlatformsChipInput
import com.gitlab.valerykameko.computergameshandbook.views.TagsChipInput
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.math.min
import kotlin.math.max

class GamesFilterFragment : Fragment() {
    private lateinit var tagsInput: TagsChipInput
    private lateinit var genresInput: GenresChipInput
    private lateinit var platformsInput: PlatformsChipInput

    private lateinit var binding: FragmentGamesFilterBinding
    private val viewModel: GamesFilterViewModel by viewModels {
        GamesFilterViewModel.Factory(
            StorageFactory.getQueryStorage(requireContext()),
            StorageFactory.getTagsStorage(requireContext()),
            StorageFactory.getGenresStorage(requireContext()),
            StorageFactory.getPlatformsStorage(requireContext()),
        )
    }

    private var loadTagsJob: Job? = null
    private var loadGenresJob: Job? = null
    private var loadPlatformsJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) : View? {
        binding = DataBindingUtil.inflate<FragmentGamesFilterBinding>(
            inflater, R.layout.fragment_games_filter, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
            gamesFilterViewModel = viewModel
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())

            this@GamesFilterFragment.tagsInput = tagsInput
            this@GamesFilterFragment.genresInput = genresInput
            this@GamesFilterFragment.platformsInput = platformsInput

            toolbar.setNavigationOnClickListener { view -> view.findNavController().navigateUp() }
            tagsInput.addTextChangedListener(this@GamesFilterFragment::loadTags)
            genresInput.addTextChangedListener(this@GamesFilterFragment::loadGenres)
            platformsInput.addTextChangedListener(this@GamesFilterFragment::loadPlatforms)

            viewModel.ratingMax.observe(viewLifecycleOwner, { ratingMax ->
                val ratingMin = viewModel.ratingMin.value!!
                if (ratingMax < ratingMin) {
                    viewModel.ratingMax.value = max(ratingMax, viewModel.ratingMin.value!!)
                }
            })
            viewModel.ratingMin.observe(viewLifecycleOwner, { ratingMin ->
                val ratingMax = viewModel.ratingMax.value!!
                if (ratingMin > ratingMax) {
                    viewModel.ratingMin.value = min(ratingMin, viewModel.ratingMax.value!!)
                }
            })

            clearFiltersButton.setOnClickListener { viewModel.clearFilters() }
            applyFiltersButton.setOnClickListener(this@GamesFilterFragment::applyFilters)
        }
        return binding.root
    }

    private fun loadTags(prefix: String) {
        loadTagsJob?.cancel()
        loadTagsJob = lifecycleScope.launch {
            viewModel.loadAutocompleteTags(prefix)
        }
    }

    private fun loadGenres(prefix: String) {
        loadGenresJob?.cancel()
        loadGenresJob = lifecycleScope.launch {
            viewModel.loadAutocompleteGenres(prefix)
        }
    }

    private fun loadPlatforms(prefix: String) {
        loadPlatformsJob?.cancel()
        loadPlatformsJob = lifecycleScope.launch {
            viewModel.loadAutocompletePlatforms(prefix)
        }
    }

    private fun applyFilters(view: View) {
        val addedByMin = viewModel.addedByMin.value
        val addedByMax = viewModel.addedByMax.value
        if (addedByMin != null && addedByMax != null && addedByMin > addedByMax) {
            showError(resources.getString(R.string.games_filter_error_added_not_in_order))
        } else {
            viewModel.applyFilters()
            findNavController().navigateUp()
        }
    }

    private fun showError(error: String) {
        val snackbar = Snackbar.make(
            requireView().rootView, error, Snackbar.LENGTH_SHORT
        )
        snackbar.show()
    }
}