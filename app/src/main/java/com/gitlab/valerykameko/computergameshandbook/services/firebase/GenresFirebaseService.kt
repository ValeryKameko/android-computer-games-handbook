package com.gitlab.valerykameko.computergameshandbook.services.firebase

import androidx.collection.LruCache
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GenreView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GenresQueryView
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class GenresFirebaseService(
    firebaseFirestore: FirebaseFirestore
) {
    private val genresCollection = firebaseFirestore.collection("genres")
    private val cache: LruCache<String, GenreView> = LruCache(30)

    fun getGenres(query: GenresQueryView?): Flow<List<GenreView>> = flow {
        var firebaseQuery: Query = genresCollection

        query?.let {
            firebaseQuery = query.applyQuery(firebaseQuery)
        }

        val genresSnapshot = firebaseQuery.get().await()
        val genreViews = genresSnapshot.mapNotNull { deserializeGenre(it) }

        emit(genreViews)
    }

    fun getGenre(id: String): Flow<GenreView?> = flow {
        val genreView = cache.get(id) ?: run {
            val genreDocument = genresCollection.document(id)
            val genreSnapshot = genreDocument.get().await()
            val genreView = deserializeGenre(genreSnapshot)

            Timber.i("Loaded genre $genreView")
            genreView?.also { cache.put(id, genreView) }
        }

        emit(genreView)
    }

    suspend fun addGenre(genre: GenreView): DocumentReference {
        val sameGenresQuery = genresCollection.whereEqualTo("name", genre.name)
        val sameGenresSnapshot = sameGenresQuery.get().await()
        sameGenresSnapshot.forEach { sameGenreSnapshot ->
            if (sameGenreSnapshot.exists())
                return sameGenreSnapshot.reference
        }

        val genreSnapshot = serializeGenre(genre)
        return genresCollection.add(genreSnapshot).await()
    }

    private fun deserializeGenre(document: DocumentSnapshot): GenreView? {
        if (!document.exists())
            return null
        return GenreView(
            name = document.getString("name")!!
        )
    }

    private fun serializeGenre(genre: GenreView): Map<String, Any> {
        return mapOf(
            "name" to genre.name
        )
    }
}
