package com.gitlab.valerykameko.computergameshandbook.data.model

data class Platform(
    val name: String,
)