package com.gitlab.valerykameko.computergameshandbook.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.adapters.GamesGalleryPagedAdapter
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.databinding.FragmentGamesGalleryBinding
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GamesGalleryViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class GamesGalleryFragment : Fragment() {
    private lateinit var binding: FragmentGamesGalleryBinding
    private lateinit var progressBar: ProgressBar

    private val viewModel: GamesGalleryViewModel by viewModels {
        GamesGalleryViewModel.Factory(
            StorageFactory.getGamesStorage(requireContext()),
            StorageFactory.getQueryStorage(requireContext()),
        )
    }
    private val adapter by lazy {
        GamesGalleryPagedAdapter(StorageFactory.getMediaStorage(requireContext()))
    }
    private var loadJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = DataBindingUtil.inflate<FragmentGamesGalleryBinding>(
            inflater, R.layout.fragment_games_gallery, container, false
        ).apply {
            gamesGalleryViewModel = viewModel
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())
            gamesRecyclerView.adapter = adapter
            lifecycleOwner = viewLifecycleOwner

            this@GamesGalleryFragment.progressBar = progressBar
            toolbar.setOnMenuItemClickListener(this@GamesGalleryFragment::onOptionsItemSelected)
            toolbar.setNavigationOnClickListener {
                val direction =
                    GamesGalleryFragmentDirections.actionGamesGalleryFragmentToSettingsFragment()
                findNavController().navigate(direction)
            }

            adapter.addLoadStateListener(this@GamesGalleryFragment::loadingStateHandler)
            loadData()
        }
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filterAction -> {
                val direction =
                    GamesGalleryFragmentDirections.actionGamesGalleryFragmentToGamesFilterFragment()
                findNavController().navigate(direction)
                true
            }
            R.id.addAction -> {
                val direction =
                    GamesGalleryFragmentDirections.actionGamesGalleryFragmentToNewGameFragment()
                findNavController().navigate(direction)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun loadingStateHandler(loadState: CombinedLoadStates) {
        when (loadState.refresh) {
            is LoadState.Loading -> progressBar.visibility = View.VISIBLE
            is LoadState.NotLoading -> progressBar.visibility = View.GONE
            is LoadState.Error -> {
                val snackbar = Snackbar.make(
                    requireView().rootView, R.string.generic_error, Snackbar.LENGTH_INDEFINITE
                )
                snackbar.setAction(R.string.retry) { loadData() }
                snackbar.show()
            }
        }
    }

    private fun loadData() {
        loadJob?.cancel()
        loadJob = lifecycleScope.launch {
            viewModel.loadGames().collectLatest { games ->
                adapter.submitData(games)
            }
        }
    }
}