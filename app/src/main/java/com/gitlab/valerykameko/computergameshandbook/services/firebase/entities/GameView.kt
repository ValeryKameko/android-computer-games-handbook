package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import android.net.Uri
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.PropertyName
import org.w3c.dom.Document
import java.time.Instant
import java.util.*

data class GameView(
    val id: String,
    val name: String,
    val rating: Double,
    val released: Date,
    val added: Long,
    val platforms: List<PlatformView>,
    val tags: List<TagView>,
    val genres: List<GenreView>,
    val description: String,
    val websiteSrc: Uri?,
    val imageSrc: Uri?,
    val clipSrc: Uri?,
) {
    companion object {
        fun fromView(view: GameView) = with(view) {
            Game(
                id = id,
                name = name,
                rating = rating,
                releasedDate = released,
                addedCount = added,
                platforms = platforms.map(PlatformView.Companion::fromView),
                tags = tags.map(TagView.Companion::fromView),
                genres = genres.map(GenreView.Companion::fromView),
                description = description,
                websiteUri = websiteSrc,
                imageUri = imageSrc,
                clipUri = null,//clipSrc,
            )
        }

        fun toView(game: Game) = with(game) {
            GameView(
                id = id,
                name = name,
                rating = rating,
                released = releasedDate,
                added = addedCount,
                platforms = emptyList(), //platforms.map(PlatformView.Companion::toView),
                tags = emptyList(), //tags.map(TagView.Companion::toView),
                genres = emptyList(), //genres.map(GenreView.Companion::toView),
                description = description,
                websiteSrc = websiteUri,
                imageSrc = imageUri,
                clipSrc = clipUri,
            )
        }
    }
}