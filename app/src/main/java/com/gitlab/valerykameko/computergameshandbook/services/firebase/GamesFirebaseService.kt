package com.gitlab.valerykameko.computergameshandbook.services.firebase

import android.annotation.SuppressLint
import android.net.Uri
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.Cursor
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GameView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GamesQueryView
import com.google.firebase.firestore.*
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageException.ERROR_OBJECT_NOT_FOUND
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import java.text.SimpleDateFormat

class GamesFirebaseService(
    firebaseFirestore: FirebaseFirestore,
    private val genresFirebaseService: GenresFirebaseService,
    private val platformsFirebaseService: PlatformsFirebaseService,
    private val tagsFirebaseService: TagsFirebaseService,
) {
    companion object {
        private const val RELEASED_DATE_FORMAT = "yyyy-MM-dd";
    }

    private val gamesCollection = firebaseFirestore.collection("games")

    fun getGames(
        query: GamesQueryView? = null,
        cursor: DocumentSnapshot? = null,
        count: Long? = null,
    ): Flow<Cursor<GameView>> = flow {
        val gameViews = mutableListOf<GameView>()
        var currentCursor = cursor
        do {
            var firebaseQuery: Query = gamesCollection
            query?.let {
                firebaseQuery = query.applyQuery(firebaseQuery)
            }

            currentCursor?.let { currentCursor ->
                firebaseQuery = firebaseQuery.startAfter(currentCursor)
            }

            count?.let { count ->
                firebaseQuery = firebaseQuery.limit(count)
            }

            val gamesSnapshot = firebaseQuery.get().await()
            currentCursor = gamesSnapshot.documents.lastOrNull()
            gameViews += gamesSnapshot
                .mapNotNull { deserializeGame(it) }
                .filter { query?.match(it) ?: true }
        } while (currentCursor != null && (gameViews.count() < (count ?: Long.MAX_VALUE)))

        emit(Cursor(gameViews, currentCursor))
    }.flowOn(Dispatchers.IO)

    fun getGame(id: String): Flow<GameView?> = flow<GameView?> {
        val gameDocument = gamesCollection.document(id).get().await()
        val gameView = deserializeGame(gameDocument)
        emit(gameView)
    }.catch { error ->
        if (error is StorageException && error.errorCode == ERROR_OBJECT_NOT_FOUND) {
            emit(null)
        } else {
            throw error
        }
    }.flowOn(Dispatchers.IO)

    suspend fun addGame(game: GameView): DocumentReference {
        val game = serializeGame(game)
        return gamesCollection.add(game).await().also {
            Timber.i("Created firebase game ${it.path}: ${game}")
        }
    }

    suspend fun removeGame(id: String) {
        val gameReference = gamesCollection.document(id)
        try {
            gameReference.delete().await()
        } catch (ex: StorageException) {
        }
    }

    @SuppressLint("SimpleDateFormat")
    private suspend fun deserializeGame(document: DocumentSnapshot): GameView? {
        if (!document.exists())
            return null
        val platforms = (document.get("platforms") as List<*>)
            .filterIsInstance<DocumentReference>().flatMap {
                platformsFirebaseService.getPlatform(it.id).toCollection(mutableListOf())
            }
        val tags = (document.get("tags") as List<*>)
            .filterIsInstance<DocumentReference>().flatMap {
                tagsFirebaseService.getTag(it.id).toCollection(mutableListOf())
            }
        val genres = (document.get("genres") as List<*>)
            .filterIsInstance<DocumentReference>().flatMap {
                genresFirebaseService.getGenre(it.id).toCollection(mutableListOf())
            }
        return GameView(
            id = document.id,
            name = document.getString("name")!!,
            rating = document.getDouble("rating")!!,
            released = SimpleDateFormat(RELEASED_DATE_FORMAT).parse(document.getString("released")!!)!!,
            added = document.getLong("added")!!,
            platforms = platforms.filterNotNull(),
            tags = tags.filterNotNull(),
            genres = genres.filterNotNull(),
            description = document.getString("description")!!,
            websiteSrc = document.getString("website")?.let { Uri.parse(it) },
            imageSrc = document.getString("image")?.let { Uri.parse(it) },
            clipSrc = document.getString("clip")?.let { Uri.parse(it) },
        )
    }

    @SuppressLint("SimpleDateFormat")
    private suspend fun serializeGame(game: GameView): Map<String, Any?> {
        val platforms = game.platforms.map { platform ->
            platformsFirebaseService.addPlatform(platform)
        }
        val genres = game.genres.map { genre ->
            genresFirebaseService.addGenre(genre)
        }
        val tags = game.tags.map { tag ->
            tagsFirebaseService.addTag(tag)
        }
        return mapOf(
            "name" to game.name,
            "rating" to game.rating,
            "released" to SimpleDateFormat(RELEASED_DATE_FORMAT).format(game.released),
            "added" to game.added,
            "platforms" to platforms,
            "tags" to tags,
            "genres" to genres,
            "description" to game.description,
            "website" to game.websiteSrc?.toString(),
            "image" to game.imageSrc?.toString(),
            "clip" to game.clipSrc?.toString(),
        )
    }
}