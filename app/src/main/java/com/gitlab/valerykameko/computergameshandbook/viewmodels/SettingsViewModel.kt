package com.gitlab.valerykameko.computergameshandbook.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.valerykameko.computergameshandbook.data.SettingsStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.Language
import com.gitlab.valerykameko.computergameshandbook.data.model.Settings
import com.gitlab.valerykameko.computergameshandbook.services.FontsService
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry

class SettingsViewModel(
    private val settingsStorage: SettingsStorage,
    fontsService: FontsService,
) : ViewModel() {
    private val settings = settingsStorage.getSettings()

    val fontEntries = listOf(FontEntry.DEFAULT_FONT_ENTRY) + fontsService.getAvailableFonts()
    val fontEntry = MutableLiveData(settings.value!!.fontEntry)

    val languages = Language.values().toList()
    val language = MutableLiveData(settings.value!!.language)

    fun saveSettings() {
        settingsStorage.setSettings(Settings(
            fontEntry = fontEntry.value!!,
            language = language.value!!,
        ))
    }

    fun clearSettings() {
        fontEntry.value = settings.value!!.fontEntry
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val settingsStorage: SettingsStorage,
        private val fontsService: FontsService,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            SettingsViewModel(settingsStorage, fontsService) as T
    }
}
