package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.gitlab.valerykameko.computergameshandbook.data.model.PlatformQuery
import com.gitlab.valerykameko.computergameshandbook.services.FirebaseServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.PlatformView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.PlatformsQueryView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber

class PlatformsStorage(
    context: Context,
) {
    companion object {
        private const val MAX_PLATFORMS_COUNT_SIZE = 50
    }

    private val platformsFirebaseService by lazy { FirebaseServiceFactory.getPlatformsFirebaseService(context) }

    fun getPlatforms(query: PlatformQuery?): Flow<List<Platform>> {
        Timber.d("Loading $query")
        return platformsFirebaseService.getPlatforms(
            query = query?.copy(
                count = Integer.min(query.count, MAX_PLATFORMS_COUNT_SIZE)
            )?.let(::PlatformsQueryView)
        ).map {  items ->
            Timber.d("Loaded $items")
            items.map(PlatformView.Companion::fromView)
        }
    }
}

