package com.gitlab.valerykameko.computergameshandbook.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gitlab.valerykameko.computergameshandbook.data.MediaStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class GalleryGameViewModel(
    game: Game,
    private var mediaStorage: MediaStorage,
) : ViewModel() {
    private val releasedDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.getAvailableLocales()[0])

    val id = game.id
    val title = game.name
    val description = game.description
    val rating = game.rating.toFloat()
    val releasedDate = releasedDateFormat.format(game.releasedDate)
    val addedCount = game.addedCount

    val genres = game.genres
    val bitmap: LiveData<Bitmap?> = game.imageUri.let { imageUri ->
        Timber.d("Should load image $imageUri")
        imageUri?.let(mediaStorage::fetchImage) ?: MutableLiveData(null)
    }
    val bitmapUri = game.imageUri.let { imageUri ->
        imageUri?.let(mediaStorage::fetchImage) ?: MutableLiveData(null)
    }
}
