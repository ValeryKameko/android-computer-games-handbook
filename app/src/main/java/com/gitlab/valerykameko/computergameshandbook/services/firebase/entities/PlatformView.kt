package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Platform

data class PlatformView(
    val name: String,
) {
    companion object {
        fun fromView(view: PlatformView) = Platform(view.name)
        fun toView(platform: Platform) = PlatformView(platform.name)
    }
}

