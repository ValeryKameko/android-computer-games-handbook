package com.gitlab.valerykameko.computergameshandbook.util

import android.annotation.TargetApi
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import com.gitlab.valerykameko.computergameshandbook.data.model.Language
import java.util.*

/*
    Based on https://stackoverflow.com/a/40704077
    Thanks https://stackoverflow.com/users/2199894/bassel-mourjan
    */
class LanguageContextWrapper internal constructor(base: Context?) : ContextWrapper(base) {
    companion object {
        fun wrap(context: Context, language: Language): ContextWrapper {
            var context: Context = context
            val config: Configuration = context.resources.configuration
            val sysLocale: Locale = getSystemLocale(config)

            if (sysLocale.language != language.language) {
                val locale = language.locale
                Locale.setDefault(locale)
                setSystemLocale(config, locale)
            }

            context = context.createConfigurationContext(config)
            return LanguageContextWrapper(context)
        }

        @TargetApi(Build.VERSION_CODES.N)
        fun getSystemLocale(config: Configuration): Locale {
            return config.locales.get(0)
        }

        @TargetApi(Build.VERSION_CODES.N)
        fun setSystemLocale(config: Configuration, locale: Locale?) {
            config.setLocale(locale)
        }
    }
}