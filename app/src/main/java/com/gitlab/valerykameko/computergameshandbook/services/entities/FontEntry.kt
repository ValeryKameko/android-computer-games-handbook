package com.gitlab.valerykameko.computergameshandbook.services.entities

import android.graphics.Typeface
import android.net.Uri

data class FontEntry(
    val name: String,
    val typeface: Typeface,
    val uri: Uri?,
) {
    companion object {
        val DEFAULT_FONT_ENTRY = FontEntry(
            name = "DEFAULT",
            typeface = Typeface.DEFAULT,
            uri = null,
        )
    }

    override fun toString() = name
}