package com.gitlab.valerykameko.computergameshandbook.viewmodels

import androidx.lifecycle.*
import com.gitlab.valerykameko.computergameshandbook.data.GamesQueryStorage
import com.gitlab.valerykameko.computergameshandbook.data.GenresStorage
import com.gitlab.valerykameko.computergameshandbook.data.PlatformsStorage
import com.gitlab.valerykameko.computergameshandbook.data.TagsStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.*
import kotlinx.coroutines.flow.collect

class GamesFilterViewModel(
    private val gamesQueryStorage: GamesQueryStorage,
    private val tagsStorage: TagsStorage,
    private val genresStorage: GenresStorage,
    private val platformsStorage: PlatformsStorage,
) : ViewModel() {
    companion object {
        private const val TAGS_COUNT = 10
        private const val PLATFORMS_COUNT = 10
        private const val GENRES_COUNT = 10
        private const val MIN_RATING = 0.0f
        private const val MAX_RATING = 5.0f
    }
    private val query = gamesQueryStorage.query
    private val _autocompleteTags = MutableLiveData<List<Tag>>()
    private val _autocompleteGenres = MutableLiveData<List<Genre>>()
    private val _autocompletePlatforms = MutableLiveData<List<Platform>>()

    val titleSubstring = MutableLiveData(query.value?.titleSubstring)
    val tags = MutableLiveData<List<Tag>>(query.value?.tags)
    val autocompleteTags: LiveData<List<Tag>> = _autocompleteTags
    val platforms = MutableLiveData<List<Platform>>(query.value?.platforms)
    val autocompletePlatforms: LiveData<List<Platform>> = _autocompletePlatforms
    val genres = MutableLiveData<List<Genre>>(query.value?.genres)
    val autocompleteGenres: LiveData<List<Genre>> = _autocompleteGenres
    val addedByMin = MutableLiveData<Int?>(query.value?.minAddedBy)
    val addedByMax = MutableLiveData<Int?>(query.value?.maxAddedBy)
    val ratingMin = MutableLiveData<Float>(query.value?.minRating ?: MIN_RATING)
    val ratingMax = MutableLiveData<Float>(query.value?.maxRating ?: MAX_RATING)
    val sortingField = MutableLiveData(query.value?.sortingField?.field ?: GameField.NONE)
    val sortingOrder = MutableLiveData(query.value?.sortingField?.order ?: SortOrder.ASC)

    val sortingFields = GameField.values().asList()
    val sortingOrders = SortOrder.values().asList()

    fun applyFilters() {
        val query = GameQuery(
            titleSubstring = titleSubstring.value,
            tags = tags.value,
            genres = genres.value,
            platforms = platforms.value,
            minAddedBy = addedByMin.value,
            maxAddedBy = addedByMax.value,
            minRating = ratingMin.value,
            maxRating = ratingMax.value,
            sortingField = FieldSorting(sortingField.value!!, sortingOrder.value!!)
        )
        gamesQueryStorage.setQuery(query)
    }

    fun clearFilters() {
        titleSubstring.value = ""
        tags.value = emptyList()
        platforms.value = emptyList()
        genres.value = emptyList()
        addedByMax.value = null
        addedByMin.value = null
        ratingMin.value = MIN_RATING
        ratingMax.value = MAX_RATING
        sortingField.value = GameField.NONE
        sortingOrder.value = SortOrder.ASC
        gamesQueryStorage.clearQuery()
    }

    suspend fun loadAutocompleteTags(prefix: String) {
        tagsStorage.getTags(
            TagQuery(
                prefix = prefix,
                count = TAGS_COUNT,
            )
        ).collect { autocompleteTags ->
            _autocompleteTags.value = autocompleteTags
        }
    }

    suspend fun loadAutocompleteGenres(prefix: String) {
        genresStorage.getGenres(
            GenreQuery(
                prefix = prefix,
                count = GENRES_COUNT,
            )
        ).collect { autocompleteGenres ->
            _autocompleteGenres.value = autocompleteGenres
        }
    }

    suspend fun loadAutocompletePlatforms(prefix: String) {
        platformsStorage.getPlatforms(
            PlatformQuery(
                prefix = prefix,
                count = PLATFORMS_COUNT,
            )
        ).collect { autocompletePlatforms ->
            _autocompletePlatforms.value = autocompletePlatforms
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val gamesQueryStorage: GamesQueryStorage,
        private val tagsStorage: TagsStorage,
        private val genresStorage: GenresStorage,
        private val platformsStorage: PlatformsStorage,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            GamesFilterViewModel(
                gamesQueryStorage,
                tagsStorage,
                genresStorage,
                platformsStorage,
            ) as T
    }
}
