package com.gitlab.valerykameko.computergameshandbook.data.model

data class PlatformQuery(
    val prefix: String? = null,
    val count: Int,
)