package com.gitlab.valerykameko.computergameshandbook.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.adapters.FontEntriesArrayAdapter
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.databinding.FragmentSettingsBinding
import com.gitlab.valerykameko.computergameshandbook.services.ServiceFactory
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.SettingsViewModel

class SettingsFragment : Fragment() {
    companion object {
        private const val FONTS_DIALOG_TAG = "fontsDialog"
        private const val LANGUAGES_DIALOG_TAG = "languagesDialog"
    }

    private lateinit var binding: FragmentSettingsBinding

    private val viewModel: SettingsViewModel by viewModels {
        SettingsViewModel.Factory(
            StorageFactory.getSettingsStorage(requireContext()),
            ServiceFactory.getFontsService(requireContext())
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View? {
        binding = DataBindingUtil.inflate<FragmentSettingsBinding>(
            inflater, R.layout.fragment_settings, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
            settingsViewModel = viewModel
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())

            fontSpinnerTextView.setOnClickListener {
                val dialog = FontDialogFragment(viewModel.fontEntries)
                dialog.setListener { fontEntry ->
                    viewModel.fontEntry.value = fontEntry
                }
                dialog.show(requireActivity().supportFragmentManager, FONTS_DIALOG_TAG)
            }

            languagesSpinnerTextView.setOnClickListener {
                val dialog = LanguageDialogFragment(viewModel.languages)
                dialog.setListener { language ->
                    viewModel.language.value = language
                }
                dialog.show(requireActivity().supportFragmentManager, LANGUAGES_DIALOG_TAG)
            }

            saveButton.setOnClickListener {
                viewModel.saveSettings()
                findNavController().navigateUp()
            }

            clearButton.setOnClickListener {
                viewModel.clearSettings()
            }

            toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        }
        return binding.root
    }
}
