package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gitlab.valerykameko.computergameshandbook.data.model.Language
import com.gitlab.valerykameko.computergameshandbook.data.model.Settings
import com.gitlab.valerykameko.computergameshandbook.services.ServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry
import com.gitlab.valerykameko.computergameshandbook.services.entities.SettingsView
import java.util.*

class SettingsStorage internal constructor(
    private val context: Context,
) {
    companion object {
        private val DEFAULT_SETTINGS = Settings(
            fontEntry = FontEntry.DEFAULT_FONT_ENTRY,
            language = Language.ENGLISH,
        )
    }

    fun interface OnSettingsChangedListener {
        fun onChangeSettings(settings: Settings)
    }

    private val settingsService by lazy { ServiceFactory.getSettingsService(context) }
    private val fontsService by lazy { ServiceFactory.getFontsService(context) }

    private var _listener: OnSettingsChangedListener? = null
    private val _settings = MutableLiveData(
        settingsService.loadSettings().let(this::convertToSettings)
            .also { setLanguage(it.language) }
    )

    fun setListener(listener: OnSettingsChangedListener) {
        _listener = listener
    }

    fun getSettings(): LiveData<Settings> = _settings
    fun setSettings(settings: Settings) {
        settingsService.saveSettings(convertToSettingsView(settings))
        _settings.value = settings
        setLanguage(settings.language)
        _listener?.onChangeSettings(settings)
    }

    private fun setLanguage(language: Language) {
        Locale.setDefault(language.locale)
        val configuration = context.resources.configuration
        configuration.setLocale(language.locale)
        context.createConfigurationContext(configuration)
    }

    private fun convertToSettings(settingsView: SettingsView?) = when (settingsView) {
        null -> DEFAULT_SETTINGS
        else -> Settings(
            fontEntry = settingsView.font?.let(fontsService::getFont) ?: DEFAULT_SETTINGS.fontEntry,
            language = settingsView.language?.let(Language.Companion::fromLanguage) ?: DEFAULT_SETTINGS.language,
        )
    }

    private fun convertToSettingsView(settings: Settings) = SettingsView(
        font = settings.fontEntry.uri,
        language = settings.language.language,
    )
}