package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.GenreQuery

data class GenresQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(genreQuery: GenreQuery) : this(
        prefix = genreQuery.prefix ?: "",
        count = genreQuery.count,
    )

    fun match(genreView: GenreView) = when {
        !genreView.name.startsWith(prefix, ignoreCase) -> false
        else -> true
    }

}