package com.gitlab.valerykameko.computergameshandbook.util

sealed class Result<out T>(val error: Throwable? = null) {
    object Loading : Result<Nothing>()
    data class Success<out T>(val data: T) : Result<T>()
    class Error(error: Throwable?) : Result<Nothing>(error)
}