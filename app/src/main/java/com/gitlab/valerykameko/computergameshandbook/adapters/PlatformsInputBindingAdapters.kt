package com.gitlab.valerykameko.computergameshandbook.adapters

import androidx.databinding.*
import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.gitlab.valerykameko.computergameshandbook.views.AutocompleteChipInput
import com.gitlab.valerykameko.computergameshandbook.views.PlatformsChipInput

@InverseBindingMethods(
    InverseBindingMethod(type = PlatformsChipInput::class, attribute = "platforms"),
)
object PlatformsInputBindingAdapters {
    @JvmStatic
    @BindingAdapter("platforms")
    fun AutocompleteChipInput.bindPlatforms(platforms: List<Platform>?) =
        setValues(platforms?.map(Platform::name))

    @JvmStatic
    @InverseBindingAdapter(attribute = "platforms")
    fun AutocompleteChipInput.inverseBindPlatforms(): List<Platform> =
        getValues().map(::Platform)

    @JvmStatic
    @BindingAdapter("platformsAttrChanged")
    fun AutocompleteChipInput.bindPlatformsListener(inverseBindingListener: InverseBindingListener?) =
        setListener { _, _ -> inverseBindingListener?.onChange() }

    @JvmStatic
    @BindingAdapter("autocompletePlatforms")
    fun AutocompleteChipInput.bindAutocompletePlatforms(autocompletePlatforms: List<Platform>?) =
        setAutocompleteTags(autocompletePlatforms?.map(Platform::name))
}