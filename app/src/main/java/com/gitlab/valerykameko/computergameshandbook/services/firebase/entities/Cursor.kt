package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.google.firebase.firestore.DocumentSnapshot

data class Cursor<T>(
    val items: List<T>,
    val nextKey: DocumentSnapshot?,
)