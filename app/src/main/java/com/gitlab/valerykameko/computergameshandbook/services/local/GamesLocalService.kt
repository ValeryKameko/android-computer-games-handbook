package com.gitlab.valerykameko.computergameshandbook.services.local

import android.net.Uri
import com.gitlab.valerykameko.computergameshandbook.services.UriTypeAdapter
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.Chunk
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.GameView
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.GamesQueryView
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.RangeQuery
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.File

@ExperimentalCoroutinesApi
class GamesLocalService internal constructor(
    private val file: File,
) {
    private val games = MutableStateFlow(mutableListOf<GameView>())

    init {
        games.value = loadGames()
    }

    fun getGames(query: GamesQueryView? = null, rangeQuery: RangeQuery? = null): Flow<Chunk<GameView>> = games.map { games ->
        val gamesSequence = games.asSequence()
        val filteredGames = query?.let {
            gamesSequence.filter(query::match)
        } ?: gamesSequence
        val processedGames= query?.sorting?.let {
            val comparator = query.comparator
            filteredGames.sortedWith(comparator)
        } ?: filteredGames

        val offset = rangeQuery?.offset ?: 0
        val count = rangeQuery?.count ?: processedGames.count()

        Timber.d("Request $rangeQuery")
        val gamesRange = processedGames
            .drop(offset)
            .take(count)
            .toList()
        Chunk(
            count = games.size,
            items = gamesRange,
        )
    }

    fun getGame(id: String): Flow<GameView?> = games.map { games ->
        val game = games.find { it.id == id }
        Timber.d("Load $game")
        game
    }

    fun addGame(gameView: GameView) {
        games.value.add(gameView)
        games.value = games.value
        saveGames(games.value)
    }

    fun removeGame(id: String) {
        games.value.removeIf { it.id == id }
        games.value = games.value
        saveGames(games.value)
    }

    private fun loadGames(): MutableList<GameView> =
        file.inputStream().let { stream ->
            JsonReader(stream.reader()).use { reader ->
                val gameViewsType = object : TypeToken<List<GameView>>() {}.type
                val gson = GsonBuilder()
                    .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
                    .create()
                val gameViewsList: List<GameView> = gson.fromJson(reader, gameViewsType)
                gameViewsList.toMutableList()
            }
        }

    private fun saveGames(gameViews: List<GameView>) {
        file.outputStream().let { stream ->
            JsonWriter(stream.writer()).use { writer ->
                val gameViewsType = object : TypeToken<List<GameView>>() {}.type
                val gson = GsonBuilder()
                    .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
                    .create()
                gson.toJson(gameViews, gameViewsType, writer)
            }
        }
    }
}

