package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.FieldSorting
import com.gitlab.valerykameko.computergameshandbook.data.model.FieldsSorting
import com.gitlab.valerykameko.computergameshandbook.data.model.GameField
import com.gitlab.valerykameko.computergameshandbook.util.FieldSortingView
import com.gitlab.valerykameko.computergameshandbook.util.FieldsSortingView
import com.gitlab.valerykameko.computergameshandbook.util.SortingOrderView

class GameFieldSortingView(
    fieldSorting: FieldSorting<GameField>
) : FieldSortingView<GameFieldView, GameView>(
    field = GameFieldView.toView(fieldSorting.field),
    order = SortingOrderView.toView(fieldSorting.order),
) {
    override val comparator : Comparator<GameView> by lazy {
        val plainComparator = Comparator<GameView> { o1, o2 -> compareValuesBy(o1, o2, field::selectValue) }
        when (order) {
            SortingOrderView.ASC -> plainComparator
            SortingOrderView.DESC -> plainComparator.reversed()
        }
    }
}

class GameFieldsSortingView(
    fieldsSorting: FieldsSorting<GameField>
) : FieldsSortingView<GameFieldView, GameView>(
    fieldsSorting = fieldsSorting.fieldsSorting.map(::GameFieldSortingView)
) {
    companion object {
        private val NO_ORDER_COMPARATOR = Comparator<GameView> { _, _ -> 0}
    }

    override val comparator : Comparator<GameView> by lazy {
        val comparators = this.fieldsSorting.map { it.comparator }
        comparators.fold(NO_ORDER_COMPARATOR) { acc, comparator ->
            acc.then(comparator)
        }
    }
}