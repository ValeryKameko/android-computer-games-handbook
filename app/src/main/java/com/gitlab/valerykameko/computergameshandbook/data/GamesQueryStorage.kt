package com.gitlab.valerykameko.computergameshandbook.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gitlab.valerykameko.computergameshandbook.data.model.GameQuery

class GamesQueryStorage {
    private val _query = MutableLiveData<GameQuery>(null)
    val query: LiveData<GameQuery> = _query

    fun setQuery(query: GameQuery) {
        _query.value = query
    }

    fun clearQuery() {
        _query.value = null
    }
}