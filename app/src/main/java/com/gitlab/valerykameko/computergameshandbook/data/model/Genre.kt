package com.gitlab.valerykameko.computergameshandbook.data.model

data class Genre(
    val name: String,
)